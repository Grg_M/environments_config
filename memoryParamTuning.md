# Collection of resources on memory management tuning in a linux box

It all started debugging a possible bug that makes the writing to a USB stick
formatted in FAT32 extremely slow

1. There are many parameters that can be tuned based on specific needs and
   tasks (e.g. database on server or normal desktop use)

2. the two parameter I started tois to alter the dirty 

- https://documentation.suse.com/sles/15-SP1/html/SLES-all/cha-tuning-memory.html
- https://documentation.suse.com/sles/15-SP1/html/SLES-all/cha-util.html#sec-util-memory-meminfo
- https://stackoverflow.com/questions/27900221/difference-between-vm-dirty-ratio-and-vm-dirty-background-ratio
- https://askubuntu.com/questions/1117832/copying-files-from-pc-to-pendrive-gets-stuck-at-the-end-on-ubuntu-16-04
- https://wiki.archlinux.org/index.php/Sysctl#Virtual_memory
- https://unix.stackexchange.com/questions/375600/how-to-enable-and-use-the-bfq-scheduler
- https://www.suse.com/support/kb/doc/?id=000017857
