"""""""""""""""""""""""""""""
"""""	VIMRC OPTIONS
"""""""""""""""""""""""""""""

" for details of each options just type :help 'optionname'


" Set to auto read when a file is changed from the outside
set autoread

" Set the system clipboard as default register (disable it if multiple
" registers are needed)
set clipboard=unnamedplus



"""""""""""""""""""""""""""""
"""""	VIM UI
"""""""""""""""""""""""""""""

" Enable syntax highlighting
syntax enable

" Display line numbers on the left
set number

" visual autocomplete for command menu
set wildmenu

" set the command window height to 2 lines, to avoid many cases of having to press enter to continue
set cmdheight=2

" Make the default file explorer netrw like nerdtree
" let g:netrw_banner = 0
let g:netrw_liststyle = 1
let g:netrw_browse_split = 4
let g:netrw_altv = 1
let g:netrw_winsize = 25
" augroup ProjectDrawer
"   autocmd!
"   autocmd VimEnter * :Vexplore
" augroup END


"""""""""""""""""""""""""""""
"""""	SPACES AND TABS
"""""""""""""""""""""""""""""

" number of visual spaces per TAB
set tabstop=4

" number of  whitespaces are actually inserted per tab when editing
set softtabstop=4

" set number of whitespaces to insert  or remove using indentation
set shiftwidth=4

" be smart with tab: go to the next indent of the next tabstop when the cursor
" is at the beginning of a line
set smarttab

" Set tabstop, softtabstop and shiftwidth to the same value
" set tabstop = softtabstop = shiftwidth =4

" convert tabs into the number of spaces that was set before
"set expandtab

" wrap lines visually (the line is still one line of text, but Vim displays it on multiple lines)
set wrap

" set the automatic indent indentation of the current line to the next
set autoindent

" set indentation to reacts to the syntax/style of the code that is being edited
set smartindent

" Use linebreak to break at word boundaries
set linebreak

" Use columns to maintain a visual width narrower than the size of your screen
" set columns=120

" Use textwidth to enforce hard line breaks (For example, the verbose portion
" of a Git commit should we no wider than 72 characters - this allows for git
" log padding, and centralises the message
" set textwidth=72

" set a visual margin to help reformat the text
set colorcolumn=120

" set color of the colorcolumn
highlight ColorColumn ctermbg=magenta

" set indentation lines
" set listchars=tab:\|\
" set list



"""""""""""""""""""""""""""""
"""""	SEARCHING
"""""""""""""""""""""""""""""

" search as characters are entered
set incsearch

" highlight matches
set hlsearch

" ignore case when searching except when using capital letters
set ignorecase
set smartcase

" For regular expressions turn magic on
set magic



"""""""""""""""""""""""""""""
"""""	FILES AND BACKUPS
"""""""""""""""""""""""""""""

" make vim recognize .md as markdown file
au BufNewFile,BufFilePre,BufRead *.md set filetype=markdown

" use filetype plugins
:filetype plugin on

" use filetype indentation:
" loads the corresponding indentation file for that filetype
filetype indent on



"""""""""""""""""""""""""""""
"""""	FUNCTIONS
"""""""""""""""""""""""""""""

" Cite as you write Better BibTex for Zotero

function! ZoteroCite()
  " pick a format based on the filetype (customize at will)
  let format = &filetype =~ '.*tex' ? 'citep' : 'pandoc'
  let api_call = 'http://127.0.0.1:23119/better-bibtex/cayw?format='.format.'&brackets=1'
  let ref = system('curl -s '.shellescape(api_call))
  return ref
endfunction

noremap <leader>z "=ZoteroCite()<CR>p
inoremap <C-z> <C-r>=ZoteroCite()<CR>



"""""""""""""""""""""""""""""
"""""	PLUGINS
"""""""""""""""""""""""""""""

" Init plugin section in Vim_Plug
" Make sure you use single quotes
call plug#begin('~/.vim/plugged')


" List of Plugins
""""""""""""""""""""

" Vim-pandoc
Plug 'vim-pandoc/vim-pandoc'

" Vim-pandoc-syntax
Plug 'vim-pandoc/vim-pandoc-syntax'

" Vim-pandoc-after
Plug 'vim-pandoc/vim-pandoc-after'

" Vim table mode
Plug 'dhruvasagar/vim-table-mode'

" minimap for vim
Plug 'wfxr/minimap.vim'

" git integration
Plug 'airblade/vim-gitgutter'

" Indent lines
Plug 'Yggdroot/indentLine'

" Autocompletion
Plug 'neoclide/coc.nvim', {'branch': 'release'}

" DevIcons
Plug 'ryanoasis/vim-devicons'

" Fuzzy Search
Plug 'junegunn/fzf', { 'do': { -> fzf#install() } }

" Trailing spaces
Plug 'ntpeters/vim-better-whitespace'


" End plugin section in Vim-Plug
call plug#end()

" vim-pandoc
" for all technical info :help vim-pandoc-settings
let g:pandoc#modules#disabled = ["folding"]
let g:pandoc#formatting#mode = 'h'

" indent lines settings
let g:indentLine_char = '|'
let g:indentLine_color_term = '283'

" auto start minimap
let g:minimap_auto_start=1
